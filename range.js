/* rangeElement v0.1.0 - Copyright (c) 2010 - 2015 BelVG LLC. (http://www.belvg.com) */
(function($) {
    jQuery.fn.rangeElement = function(options) {
        options = jQuery.extend({
            max: 10000,
            min: 0,
            increment: 1,
            readonly: false,
            wrapclass: 'range-input-element-wrap',
            button_html: '<span class="type-number"></span>',
            buttons: {
                increase: {
                    'class': 'plus',
                    'content': '&#9650;',
                    'function': 'increaseValue'
                },
				decrease: {
                    'class': 'minus',
                    'content': '&#9660;',
                    'function': 'decreaseValue'
                }
            }
        }, options);
        
        $(this).wrap("<div class='" + options.wrapclass + "'></div>");
        var elnt = $(this).parent();
        var input = $(this);
        
        var onchange = document.createEvent('HTMLEvents');
        onchange.initEvent('change', false, true);
        input.attr('type', 'number');
        if (options.readonly) {
            input.attr('readonly', true);
        };
        
        var prevValue = input.val();
        for (var button in options.buttons) {
            $(options.button_html)
            .appendTo(elnt)
            .html(options.buttons[button]['content'])
            .addClass(options.buttons[button]['class'])
            .click( function(event){
                buttonClick(event);
            });
        };
        
        input.bind('change', function() {
            if (input.val() == '' || !validateVal(input.val())) {
                input.val(prevValue);
            } else {
                prevValue = input.val();
            }
        });
        
        var buttonClick	= function(event) {
            for (var button in options.buttons) {
                if ($(event.target).hasClass(options.buttons[button]['class'])) {
                    eval(options.buttons[button]['function'] + "()");
                    console.log(input.get(0));
                    input.get(0).dispatchEvent(onchange);
                }
            }
        };
        
        var validateVal = function(newVal, oldVal) {
            if (newVal <= options.max && newVal >= options.min) {
                return newVal;
            };
            
            return oldVal;
        };
        
        var increaseValue = function() {
            var val = Number(input.val());
            val += Number(options.increment);
            input.val(validateVal(val, Number(input.val())));
        };
        
        var decreaseValue = function() {
            var val = Number(input.val());
            val -= Number(options.increment);
            input.val(validateVal(val, Number(input.val())));   
        };
    }
})(jQuery);

jQuery(document).ready( function() {
    jQuery(".range-input-element").each( function() {
        jQuery(this).rangeElement({max: 100});
    });
});